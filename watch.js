function parseQuery(search) {
    var args = search.substring(1).split('&');
    var argsParsed = {};

    var i, arg, kvp, key, value;
    for (i=0; i < args.length; i++) {

        arg = args[i];
        if (-1 === arg.indexOf('=')) {
            argsParsed[decodeURIComponent(arg).trim()] = true;
        } else {
            kvp = arg.split('=');
            key = decodeURIComponent(kvp[0]).trim();
            value = decodeURIComponent(kvp[1]).trim();
            argsParsed[key] = value;
        }
    }

    return argsParsed;
}

var survivorVideos = [];
function autoPlaySurvivors(e) {
    var actionMap = {};
    actionMap[YT.PlayerState.PAUSED] = 'pauseVideo';
    actionMap[YT.PlayerState.ENDED] = 'stopVideo';
    actionMap[YT.PlayerState.PLAYING] = 'playVideo';

    var code = e.key;
    var value = e.newValue;

    if(code in actionMap && value === 'true') {
        localStorage.setItem(code, false);

        var action = actionMap[code];
        survivorVideos.forEach(function (player) {
            player[action].call(player);
        });
    }
}

function setupVideos() {
    $.get('episodes.json', function (data) {
        var args = parseQuery(document.location.search);

        var targetId = args.v;
        var targetPerspective = args.p;

        var episode = data.find(function (d) { return d.id === targetId; });

        $('#viewport-killer').append($('<div class="videoWrapper" id="vwk"></div>'));

        if (targetPerspective === 'all' || targetPerspective === 'killer') {
            var killerPlayer = new YT.Player('vwk', {
                playerVars: {
                    autoplay: 1,
                    controls: 1,
                    start: 0
                },
                videoId: episode.killer,
                events: {
                    'onStateChange': function (event) {
                        var actionMap = {};
                        actionMap[YT.PlayerState.PAUSED] = 'pauseVideo';
                        actionMap[YT.PlayerState.ENDED] = 'stopVideo';
                        actionMap[YT.PlayerState.PLAYING] = 'playVideo';

                        var code = event.data;
                        if(code in actionMap) {
                            localStorage.setItem(code, 'true');
                            autoPlaySurvivors({
                                key: code,
                                newValue: 'true'
                            });
                        }
                    }
                }
            });
        }

        if(targetPerspective === 'all' || targetPerspective === 'survivors') {
            episode.survivors.forEach(function (p, i) {
                $('#viewport-survivors').append($('<div class="videoWrapper" id="vw' + i + '"></div>'));
                var survivorPlayer = new YT.Player('vw' + i, {
                    playerVars: {
                        autoplay: 1,
                        controls: 0
                    },
                    videoId: p,
                    events: {
                        'onReady': function (event) {
                            event.target.mute();
                            event.target.pauseVideo();
                        }
                    }
                });

                survivorVideos.push(survivorPlayer);
            });

            window.addEventListener('storage', function (e) {
                var dlg = document.querySelector('dialog');

                if(dlg.open)
                    dlg.close();
                    
                autoPlaySurvivors(e);
            });

            if(targetPerspective === 'survivors') {
                var dlg = document.querySelector('dialog');
                dlg.showModal();
            }
        }

        if(targetPerspective !== 'all') {
            if(targetPerspective === 'killer') {
                $('#viewport-survivors').hide();
                $('#viewport-killer').css('height', '100vh');
                $('#viewport-killer').css('width', '100vw');
            } else if (targetPerspective === 'survivors') {
                $('#viewport-killer').hide();
                $('#viewport-survivors').css('height', '100vh');
                $('#viewport-survivors').css('width', '100vw');
            }
        }
    }, 'json');
}

function onYouTubeIframeAPIReady() {
    setupVideos();
}
