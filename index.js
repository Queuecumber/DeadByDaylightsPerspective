$(document).ready(function () {
    $.get('episodes.json', function (data) {
        var template = $('#select-episode').html();

        data.forEach(function (d) {
            var rendered = $(Mustache.render(template, d));

            $('#episode-list').append(rendered);
        });
    }, 'json');
});
